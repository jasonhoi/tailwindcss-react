
export default function App() {
  return (
    <>

      <div className="p-4 m-4">
        
        <span className="text-5xl font-extrabold align-middle">
          <span className="bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500">
            Hello world
          </span>
        </span>

        <button className="align-middle p-2 mx-4 hover:scale-150 hover:blur-sm transition-all rounded bg-red-500 text-white shadow-xl shadow-red-400">My Button</button>

      </div>

      

    <div className="container mx-auto">
      <div className="flex">
        <div className="box md:basis-1/4 basis-1/3">01</div>
        <div className="box md:basis-2/4 basis-1/3">02</div>
        <div className="box md:basis-1/4 basis-1/3">03</div>
      </div>

      <div className="flex justify-between my-2">
        <div className="box w-20 h-14 mr-2">
          01
        </div>
        <div className="box grow mr-2">
          02
        </div>
        <div className="box w-20">
          03
        </div>
      </div>

      <div className="myalert" role="alert">
        <strong className="font-bold">Alert! </strong>
        <span className="block sm:inline">Please update your password</span>
      </div>
      
      <div className="max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl shadow-indigo-400">
        <div className="flex-shrink-0">
          <img src="logo512.png" alt="" className="h-12 w-12"/>
        </div>
        <div className="ml-6 pt-1">
          <h4 className="text-xl text-gray-900">My text media</h4>
          <p className="text-base text-gray-600">Our message</p>
        </div>
      </div>
    </div>

    </>
  )
}